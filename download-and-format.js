const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
const tabletojson = require('tabletojson').Tabletojson;

const allUrls = {
  "Bildung, Wissenschaft und Forschung": [
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umsetzung-von-medienbildungsprojekten-an-schulen-2023-2024-6136.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-schuefiapp-6131.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalangebote-im-rahmen-von-schau-rein-woche-der-offenen-unternehmen-sachsen-6130.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-bwpplus-6129.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-ausbau-der-webseite-schau-rein-zur-zentralen-saechsischen-schuelerplattform-fuer-berufliche-orientierung-6128.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-schaffen-von-lernplattformen-zur-wiederholung-von-grundlegenden-schulischen-lerninhalten-6127.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-360-grad-videos-fuer-berufsbilder-6126.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-beteiligung-an-laenderuebergreifenden-vorhaben-im-rahmen-des-digitalpakt-schule-6125.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-systematische-zielgerichtete-kommunikation-mit-stakeholdern-6124.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-initiative-breitband-zur-erschliessung-der-saechsischen-schulen-6123.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitales-testfeld-air-cargo-am-flughafen-leipzig-halle-6097.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-gestaltung-von-bauen-4-0-durch-foerderung-des-construction-future-lab-6096.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-aufbau-der-kompetenzstelle-ki-bei-der-digitalagentur-sachsen-6094.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-transparenz-bei-angeboten-und-foerderinstrumenten-im-bereich-der-allgemeinen-weiterbildung-6092.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-initiative-digitale-weiterbildung-6091.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-staerkung-der-digitalen-teilhabe-bestimmter-zielgruppen-6090.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-staerkung-der-personellen-ressourcen-anerkannter-weiterbildungseinrichtungen-zur-bewaeltigung-der-digitalen-transformation-6089.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalstrategien-der-anerkannten-weiterbildungseinrichtungen-6088.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-der-digitalen-infrastruktur-und-ausstattung-anerkannter-weiterbildungseinrichtungen-6087.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-bildung-fuer-seniorinnen-und-senioren-ueber-den-digitalpakt-alter-6083.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalkompetenz-check-6082.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalpakt-schule-2-0-in-sachsen-6081.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-federfuehrung-im-laenderuebergreifenden-projekt-intelligentes-tutorielles-system-6080.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-initiative-breitband-zur-erschliessung-der-saechsischen-schulen-6078.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-pruefung-von-foerdermoeglichkeiten-fuer-technische-ausstattung-an-berufsschulzentren-6076.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umsetzung-von-medienbildungsprojekten-an-schulen-2023-2024-6075.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-beteiligung-an-laenderuebergreifenden-vorhaben-im-rahmen-des-digitalpakt-schule-6073.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-vernetzung-und-kompetenzbildung-durch-den-simul-innovationhub-5771.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5770.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-modellprojekte-und-reallabore-als-saeule-3-des-simul-innovationhub-5769.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-wissenstransfer-zur-digitalen-transformation-im-rahmen-der-saeule-1-des-simul-innovationhub-5768.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-webportal-zum-thema-smarte-regionen-zur-unterstuetzung-saechsischer-kommunen-und-landkreise-5767.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-der-etablierung-der-nationalen-forschungsdateninfrastruktur-und-des-forschungsdatenmanagements-5766.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-forschungsprojektfoerderung-fuer-next-generation-computing-neuromorphic-computing-und-quantencomputing-5765.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-ki-strategie-fuer-den-freistaat-sachsen-5764.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-koordinierungsstelle-medienbildung-freistaat-sachsen-5763.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-etablierung-eines-monitorings-zur-umsetzung-der-strategie-zur-digitalisierung-in-der-hochschulbildung-5762.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-staerkung-der-hochschulkooperation-zur-digitalen-transformation-5761.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-plattform-digitale-hochschule-sachsen-5760.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-aufgreifen-der-digitalen-transformation-in-der-hochschulentwicklung-5759.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-dachstrategie-fuer-digitale-transformation-im-hochschulbereich-5758.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-pruefung-von-bring-your-own-device-ansaetzen-5757.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-entwicklung-einer-cloudloesung-fuer-berufliche-schulzentren-5756.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalisierungsoffensive-nachwuchsstiftung-maschinenbau-5754.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-portal-berufliche-bildung-5753.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5750.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-pruefung-von-bring-your-own-device-ansaetzen-5749.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-qualifizierung-unterrichtsfachbezogener-fachberaterinnen-und-fachberater-zur-nutzung-mobiler-endgeraete-5748.html" },
    { enabled : false, url: "https://www.digitales.sachsen.de/massnahme-konsolidierung-und-professionalisierung-der-zentralen-digitalen-dienste-des-freistaates-sachsen-fuer-den-paedagogischen-einsatz-in-saechsischen-schulen-5747.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-massnahme-aenderung-der-saechsischen-lehr-und-lernmittel-verordnung-5746.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umsetzung-der-richtlinie-digitale-schulen-5745.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-systematische-zielgerichtete-kommunikation-mit-stakeholdern-5744.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-von-e-learning-projekten-an-foerderschulen-5743.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-einfuehrung-des-leistungskurses-informatik-an-ausgewaehlten-gymnasien-5742.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-massnahme-laenderuebergreifendes-projekt-muses-5741.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-m-i-t-schulen-5740.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-projekten-zur-staerkung-der-informatischen-bildung-und-medienbildung-saechsischer-schuelerinnen-und-schueler-5739.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-schulnetzwerke-zur-staerkung-der-informatischen-bildung-in-den-regionen-westsachsen-und-ostsachsen-5738.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-schueler-endgeraeten-ab-2025-5737.html" }],
  "Gesellschaft": [
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-in-bearbeitung-6122.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-netzwerkbildung-zur-beruecksichtigung-wissenschaftlicher-grundlagen-6121.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-beirat-digitale-wertschoepfung-6100.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-weiterentwicklung-des-beteiligungsportals-6099.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-verbesserung-der-rahmenbedingungen-fuer-die-wertschoepfung-kultur-und-kreativwirtschaftlicher-unternehmen-5596.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-pilotvorhaben-zur-cross-sektoraler-zusammenarbeit-unter-nutzung-kultur-und-kreativwirtschaftlichen-know-hows-5595.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-transformation-in-den-kultur-staatsbetrieben-des-freistaates-sachsen-5594.html" },
    { enabled : false, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-von-modellprojekten-zum-verbraucherschutz-verbraucherbildung-im-rahmen-der-foerderrichtlinie-verbraucherschutz-verbraucherbildung-5592.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-bildung-fuer-seniorinnen-und-senioren-ueber-das-modellprojekt-gemeinsam-digital-2-5590.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-institutionelle-foerderung-der-verbraucherzentrale-sachsen-5589.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-kompetenzstelle-zur-digitalisierung-in-der-pflege-5588.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-online-informationsplattform-pflegenetz-sachsen-5587.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalisierung-der-saechsischen-plankrankenhaeuser-mit-einstufung-als-kritischer-infrastruktur-5586.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalisierung-von-krankenhausprozessen-in-den-saechsischen-landeskrankenhaeusern-5585.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-projekt-digitales-gesundheitsamt-2025-oegd-behoerden-des-freistaates-sachsen-5584.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umsetzung-einer-zustaendigkeitsregelung-zum-autonomen-fahren-afgbv-in-sachsen-5583.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-bereitstellung-dynamischer-verkehrsdaten-im-freistaat-sachsen-ueber-ein-online-portal-5582.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-auf-und-ausbau-von-testfeldern-fuer-intelligente-verkehrssysteme-5581.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-interessierte-buergerinnen-und-buerger-fuer-das-ehrenamt-gewinnen-5577.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-kompetenzen-fuer-das-ehrenamt-erhebung-des-weiterbildungsbedarfs-5576.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-angebot-an-weiterbildungsmoeglichkeiten-fuer-das-ehrenamt-hinsichtlich-der-digitalen-kompetenzerweiterung-5575.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-kompetenzen-fuer-das-ehrenamt-weiterbildungsangebote-5574.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-sammlung-zu-best-practice-beispielen-im-rahmen-der-foerderrichtlinie-buergerbeteiligung-5563.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-angebote-zu-online-beteiligungsformaten-5560.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-saechsisches-kinder-und-jugendbeteiligungsportal-4805.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/weitestgehende-digitalisierung-der-anerkennung-von-saat-und-pflanzgut-im-freistaat-sachsen-4710.html" }
  ],
  "Staat": [
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-weiterentwicklung-der-it-zusammenarbeit-im-verfassungsschutzverbund-6107.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-fortbildungsoffensive-mit-dem-schwerpunkt-digitalisierung-und-verwaltungsmodernisierung-6105.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-einfuehrung-eines-landeseinheitlichen-elektronischen-personalmanagementsystems-inklusive-elektronischer-personalakte-projekt-epm-sax-6103.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-ausbau-des-elektronischen-zugangs-zu-archivgut-des-saechsischen-staatsarchivs-6102.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-cybersicherheitsstrategie-sachsen-5702.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5701.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-vernetzung-der-behoerden-und-organisationen-mit-sicherheitsaufgaben-bos-5700.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-informationsarchitektur-polizei-5699.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-datenuebermittlung-in-strafsachen-an-die-justiz-durch-die-polizei-5698.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-online-anzeige-sachsen-5697.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-weiterentwicklung-des-bergbauinformationssystems-bis-5689.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalcheck-bei-normsetzungsvorhaben-5688.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-erhoehung-der-barrierefreiheit-von-verwaltungsprozessen-im-geschaeftsbereich-des-smf-5687.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-bei-der-kontinuierlichen-optimierung-von-verwaltungsprozessen-der-saechsischen-staatsverwaltung-5686.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitalisierung-der-auslaenderbehoerden-5685.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umstellung-auf-opensource-5684.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5682.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5681.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-evaluierung-des-saechsischen-informationssicherheitsgesetzes-saechsisichg-5619.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umsetzung-von-green-it-im-geschaeftsbereich-des-smwk-5617.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-green-it-strategie-fuer-den-freistaat-sachsen-5616.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-open-source-strategie-der-saechsischen-staatsverwaltung-5614.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-umstellung-auf-open-source-software-im-geschaeftsbereich-des-smwk-5613.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-open-data-als-beitrag-zu-einer-saechsischen-datenstrategie-5607.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-open-data-strategie-fuer-den-geschaeftsbereich-des-smf-5606.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-durchfuehrung-eines-ressortuebergreifenden-workshop-prozesses-im-kontext-der-erstellung-einer-saechsischen-datenstrategie-5605.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-online-karte-zur-pruefung-der-standorteignung-von-waldflaechen-fuer-windenergieanlagen-5604.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-rahmenkonzept-fuer-eine-agrardatenplattform-5603.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-weiterfuehrung-und-ausbau-des-landwirtschaft-und-umweltinformationssystems-luis-fuer-geodaten-5602.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-massnahmenpaket-zur-weiterentwicklung-von-open-data-5601.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-weiterentwicklung-des-bewerbungsportals-leo-sax-zur-lehrkraeftegewinnung-5600.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-fortsetzung-und-ausbau-des-bachelor-studiengangs-digitale-verwaltung-an-der-hochschule-meissen-fh-und-fortbildungszentrum-5599.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-novellierung-saechsisches-e-governement-gesetz-saechsegovg-5564.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-strategie-zur-digitalen-transformation-der-saechsischen-staatsverwaltung-5562.html" }
  ],
  "Wirtschaft und Arbeit": [
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-kontinuierliches-standortmarketing-fuer-den-freistaat-sachsen-betreuung-und-begleitung-von-ansiedlungs-und-investitionsvorhaben-6117.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-lokale-innovationsraeume-fuer-digitalisierung-lifd-innovationsraeume-staerken-netzwerke-erweitern-6114.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-verleihung-des-saechsischen-digitalpreises-6113.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-digitale-ertuechtigung-von-krankenhaeusern-6111.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-erarbeitung-eines-konzeptes-fuer-die-erfassung-des-standes-der-digitalen-transformation-in-der-saechsischen-wirtschaft-6110.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-beteiligung-des-freistaates-sachsen-am-europaeischen-forschungs-und-entwicklungsprogramm-key-digital-technologies-6109.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-des-esf-plus-bundesprogramms-zukunftszentren-5725.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-saechsisches-informationsportal-fuer-auslaendische-fach-und-arbeitskraeften-5724.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-pakt-zur-gewinnung-internationaler-fachkraefte-5723.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-modellprojekt-foerderung-von-praktika-fuer-internationale-zielgruppen-bei-saechsischen-unternehmen-5722.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-der-vermittlung-sozialer-kompetenzen-in-der-ausbildung-und-ausbildungsvorbereitung-5721.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-arbeitsschutz-und-digitaler-wandel-5720.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-massnahmen-zur-fachkraeftesicherung-unter-den-bedingungen-des-digitalen-wandels-5719.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-herstellung-von-transparenz-zu-themen-angeboten-und-foerderinstrumenten-im-bereich-der-beruflichen-weiterbildung-5718.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-satourn-digitalarchitektur-fuer-den-tourismus-in-sachsen-5716.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-gruenderinnen-einschliesslich-ihrer-digitalen-kompetenzen-5715.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-an-frauen-adressierte-informationsveranstaltungen-zu-foerderinstrumenten-und-angeboten-von-start-ups-5714.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-cluster-und-netzwerkfoerderung-5713.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-aufbau-cybersicherheitsnetzwerk-sachsen-csns-5712.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-digitalisierungsprojekten-fuer-kleine-und-mittlere-unternehmen-5711.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-ipcei-fuer-mikroelektronik-und-kommunikationstechnologien-5710.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-der-touristsichen-leistungstraeger-bei-der-digitalen-transformation-5709.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-deskline-digitales-informations-und-reservierungssystem-fuer-beherbergungsbetriebe-in-sachsen-5708.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-tourismusnetzwerk-sachsen-5707.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-der-saechsischen-informations-und-kommunikationstechnologie-ikt-branche-5706.html" }
  ],
  "Digitale Infrastruktur": [
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-kontinuierliche-ueberpruefung-der-rahmenbedingungen-fuer-den-gigabitausbau-im-freistaat-sachsen-6120.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-gigabitfaehiger-breitbandinfrastrukturen-im-freistaat-sachsen-6118.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-aufbau-der-verkehrszentrale-sachsen-5735.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-5734.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-task-force-mobilfunk-5733.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-aufbau-eines-saechsischen-infrastrukturatlasses-5732.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-unterstuetzung-der-kreisfreien-staedte-kommunen-und-landkreise-beim-breitbandausbau-5731.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-gigabitstrategie-5730.html" },
    { enabled : true, url: "https://www.digitales.sachsen.de/massnahme-foerderung-von-intelligenten-energiesystemen-5729.html" }
  ]
}

const items = [];
for (let dimension in allUrls) {
  console.log(dimension)

  for (let url of allUrls[dimension]) {

    if (!url.enabled) {
      console.log(`Ignoring URL ${url.url}`);
    }
    else {
      console.log(`Processing URL ${url.url}`);
      tabletojson.convertUrl(url)
        .then((tablesAsJson) => {
          const tableData = tablesAsJson[0];
          const item = {};
          tableData.forEach(function (row){
            // some stupid zero width white space character
            const key = row[0].replace(/\u200B/g,'');
            const value = row[1].replace(/\u200B/g,'');
            item[key] = value;
            item.url = url.url;
            item.dimension = dimension;
          })
          items.push(item);
          fs.writeFileSync('./strategy-items.json', JSON.stringify(items, null, 2));
        })
        .catch((err) => {
          console.log('err for url', err, url.url);
        }); // to catch error
    }
  }
}